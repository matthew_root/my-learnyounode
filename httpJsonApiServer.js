var http = require('http');
var url = require('url');

var port = Number(process.argv[2]);

var server = http.createServer(function (request, response) {
  var parsedRequest = url.parse(request.url, true);
  var endpoint = parsedRequest.pathname;
  var isoTimeString;
  var responseData;

  if (parsedRequest.query.iso) {
    isoTimeString = parsedRequest.query.iso;
  } else {
    response.writeHead(400, 'Missing iso query param');
    response.end();
  }

  if (endpoint === '/api/parsetime') {
    responseData = parseTime(isoTimeString);
  } else if (endpoint === '/api/unixtime') {
    responseData = unixTime(isoTimeString);
  }

  if (responseData) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end(JSON.stringify(responseData));
  } else {
    response.writeHead(405, 'Invalid endpoint');
    response.end();
  }
});

function parseTime(time) {
  var parsedDate = new Date(time);
  return {
    'hour': parsedDate.getHours(),
    'minute': parsedDate.getMinutes(),
    'second': parsedDate.getSeconds()
  };
}

function unixTime(time) {
  return { 'unixtime': Date.parse(time) };
}

server.listen(port);
