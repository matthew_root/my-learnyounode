var fs = require('fs');

function countNewLines(err, fileData) {
  if (err) {
    console.log('Error processing file');
    return;
  }

  var lines = fileData.toString().split('\n').length - 1;
  console.log(lines);
}

fs.readFile(process.argv[2], countNewLines);
