var http = require('http');
var map = require('through2-map');

var port = process.argv[2];

var server = http.createServer(function (request, response) {
  if (request.method !== 'POST') {
    response.writeHead(405, 'That HTTP method is not allowed');
    response.end();
  } else {
    response.writeHead(200, { 'Content-type': 'text/plain' });
    request.pipe(map(function (chunk) {
      return chunk.toString().toUpperCase();
    })).pipe(response);
  }
});

server.listen(port);
