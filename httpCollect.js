var http = require('http');

http.get(process.argv[2], function (response) {
  response.setEncoding('utf-8');

  var receivedData = '';

  response.on('data', function (data) {
    receivedData += data;
  });

  response.on('end', function () {
    console.log(receivedData.length);
    console.log(receivedData);
  })
});
