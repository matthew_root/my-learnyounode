var fs = require('fs');
var filePath;

if (process.argv.length > 2) {
  filePath = process.argv[2];
} else {
  console.log('Filepath is a required parameter');
  return;
}

var fileByLines = fs.readFileSync(filePath).toString().split('\n');
console.log(fileByLines.length - 1);
