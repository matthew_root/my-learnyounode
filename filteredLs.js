var fs = require('fs');
var inputDir, filterExtension;

if (process.argv.length > 2) {
  inputDir = process.argv[2];
  filterExtension = '.' + process.argv[3];
}

function filterFiles(err, files) {
  if (err) {
    return console.log(err);
  }

  files.forEach(function (file) {
    if (file.endsWith(filterExtension)) {
      console.log(file);
    }
  })
}

fs.readdir(inputDir, filterFiles);
