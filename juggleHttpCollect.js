var http = require('http');
var bl = require('bl');

var urls = process.argv.slice(2);
var responseData = [];
var responseCount = 0;

urls.forEach(function (url, index) {
  http.get(url, function (response) {
    response.pipe(bl(function (err, data) {
      if (err) {
        return console.error(err);
      }

      responseData[index] = data.toString();
      responseCount++;

      if (responseCount === urls.length) {
        printResponses();
      }
    }));
  })
});

function printResponses() {
  responseData.forEach(function (data) {
    console.log(data);
  })
}
