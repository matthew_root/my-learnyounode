var fs = require('fs');
var path = require('path');

var applyFilter = function (directory, extension, callback) {
  fs.readdir(directory, function (err, files) {
    if (err) {
      return callback(err);
    }

    var fullExtension = '.' + extension;

    files = files.filter(function (file) {
      return path.extname(file) === fullExtension
    });

    callback(null, files);
  })
};

module.exports = applyFilter;
