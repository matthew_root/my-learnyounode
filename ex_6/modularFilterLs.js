var applyFilter = require('./applyFilter');

if (process.argv.length > 2) {
  applyFilter(process.argv[2], process.argv[3], printFiles);
}

function printFiles(err, files) {
  if (err) {
    console.log('There was an error parsing the directory.');
  }

  files.forEach(function (file) {
    console.log(file);
  })
}
